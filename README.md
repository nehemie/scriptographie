# Scriptographie (working title)

This repository contains the work in progress of Sébastien Plutniak
and Néhémie Strupler for a contribution in Techniques & Cultures

## Structure

 - **proposal/**: submitted contribution proposal (30-05-2018)

## License

CC BY-SA 4.0
