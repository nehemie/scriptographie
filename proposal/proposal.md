---
title: Proposition de contribution au volume Technographies de la revue Techniques et Culture
---

 - **Titre** : « Transcrire la démarche scientifique : programmation lettrée, _versioning_ et reproductibilité »

 - **Auteurs** : 
    - Néhémie Strupler \
    Université de Strasbourg et Institut Français d'Études Anatoliennes \
  [nehemie.strupler@posteo.net](mailto:nehemie.strupler@posteo.net)
    - Sébastien Plutniak \
    École française de Rome \
  [sebastien.plutniak@ehess.fr](mailto:sebastien.plutniak@ehess.fr)

## Proposition de contribution

Sous bien des aspects, le développement de logiciels informatiques, et
en particulier de logiciels libres, ont des objectifs assez similaires
à ceux de l'expérimentation scientifique : fournir une description
univoque des protocoles ainsi que de l'exécution de l'expérimentation
et mettre à disposition des résultats. Il existe une analogie très
forte entre le script informatique − une suite d'instructions
composées par un humain et exécutées par une machine − et la
documentation d'une chaîne opératoire d'une expérimentation. La taille
des projets informatiques − qui peut nécessiter la collaboration de
plusieurs centaines de personnes − ainsi que la nécessité de maintenir
ces programmes à jour en les faisant évoluer régulièrement de façon
incrémentielle, a mené au développement d'instruments logiciels
spécifiques.

En sciences expérimentales, ces instruments ont été adoptés afin de
documenter et décrire les chaînes opératoires des expériences, ainsi
que retranscrire, mesurer, quantifier, archiver et transmettre les
protocoles et leurs résultats.  Leur usage a, en outre, été intégré
aux formats de publications classiques tels que les articles de revues
et les monographies depuis que ceux-ci ont été informatisés.  À
l'unicité du texte imprimé se substitue aujourd'hui la pluralité des
formats informatiques qui peuvent être obtenus automatiquement.
Ainsi, un article imprimé dans une revue peut être disponible en ligne
dans un _Portable Document Format (PDF)_ ou encore en _Hypertext
Markup Language (HTML)_, être sémantiquement décrit à partir d'une
ontologie standard (comme le CIDOC-CRM) ou, encore, être accompagné
d'un script permettant de ré-éxécuter la séquence de calculs sur
lequel s'appuie la publication. 

Dans le cas des sciences humaines, si l'informatisation des chaînes
éditoriales s'est largement généralisée, elle n'a que très faiblement
impliqué l'adoption de ces technologies. Malgré l'informatisation, les
principes des chaînes opératoires de recherches en sciences humaines
sont restées globalement similaires, sans tirer parti de ces
instruments logiciels permettant de décrire les processus de recherche
eux-mêmes. Dans cette situation d'utilisation – selon nous –
sous-optimale de ces technologies, nous souhaitons, avec cet article,
attirer l'attention sur ces potentialités pour les sciences humaines :
montrer leur intérêt pour outiller le travail collectif, pour
transcrire les procédures de recherche et, ce faisant, assurer leur
pérennité et leur reproductibilité.

Cette contribution portera en particulier sur trois instruments
logiciels présentant des potentialités importantes pour les sciences
humaines et les interactions des chercheurs avec les outils
informatiques, dorénavant incontournables y compris dans ces
disciplines : les systèmes de gestion de versions (_versioning_), la
programmation lettrée et la reproductibilité des procédures. 

La première partie mettra en évidence le rapport direct de ces
technologies avec les problèmes épistémologiques classiques concernant
l'expérimentation et leur reproduction, ainsi que la cumulativité des
connaissances en sciences humaines et sociales.
La deuxième partie montrera en quoi le script constitue une
« technographie », et examinera son analogie avec la description de
chaînes opératoires.  Une particularité de cette technographie sera
néanmoins soulignée, en l'occurrence  le fait que le script revient à
une écriture à la fois destinée à la lecture humaine et à l'exécution
automatisée par la machine.
Une troisième partie sera consacrée aux instruments logiciels qui
ont été développés ces dernières années afin d'assurer la
reproductibilité des expérimentations. 

Nous envisageons de fournir une contribution pour le premier volume,
mais nous serions tout à fait prêt à fournir également des fiches
explicatives pour le deuxième volume afin de mettre à disposition un
guide  pour la mise en place d'expériences reproductibles.



## Projet iconographique

Pour illustrer notre propos, nous envisageons la documentation
iconographique suivante dont certaines images seront reproduites pour
être mieux adaptées au public et que nous étofferons avec nos
réalisations.

![Le « Le traitement de l'information archéologique », d'après les idées de J.-C. Gardin reprise dans la brochure anonyme _Anarchéologie_ (1968)](figures/gardin2.jpg){ width=13cm }

![Schématisation idéale d'une chaîne opératoire permettant une réversibilité entre le parcours de l'auteur et celui de son lecteur.](figures/ResearchPipeline.png){ width=13cm }

![Présentation humoristique du problème des versions (©Piled Higher and Deeper Publishing, LLC)](figures/PhdComics_final.gif){ height=15cm }

![Séquence générale des évènements pour une utilisation d'un système de gestion de version. John a téléchargé le répertoire (_clone_), il fait des modifications qu'il met à disposition (_commit_). Jessica fait de même et ainsi de suite. Le logiciel assure que ce soit toujours la dernière version qui soit actualisée. CC BY-NC-SA Scott Chacon.](figures/chacon_gitbook_small-team-flow.png){ height=15cm }


<!--  les autres illustrations me semblent très complexes et risquent
d'effrayer-->


<!--

# Introduction


## le paradoxe d'une utilisation sous-optimale de technologie pourtant anciennes et éprouvées

Idée d'une utilisation généralement sous-optimale, y compris des
logiciels les plus communs comme les « traitements de texte ».
Situation relativement paradoxale dans la mesure où des technologies
puissantes existent depuis longtemps mais ne sont pas ou rarement
adoptées par les chercheurs en SH (LaTeX, gestion bibliographique).

Il ne s'agit toutefois pas ici d'étudier ce problème.

## Focus de cet article

L'article vise à présenter 3 technologies particulières :

* le suivi de version (on peut le nommer autrement)
* les principes et instruments de programmation lettrée
* les principes et instruements pour la reproductibilité


## Problématisation
Différents cadrages possibles (et complémentaires) :

* technologie culturelle, technographie : le script comme écriture
  d'une chaîne opératoire

* les (nombreux) travaux récents sur l'écriture en sciences humaines
  (NB : j'ai les réfs biblio nécessaires pour cela : génétique
  textuelle, socio pragmatique de l'écriture, etc.)



# L'instrumentation logicielle de l'écriture scientifique

## cumuler et reproduire : de vieilles idées

* en bibliographie : numérotation des éditions
* Gardin et les banques de données (NB: j'ai les matériaux
    nécessaires pour ce point)

## Les propositions documentaires de Gardin pour les sciences humaines
La généralisation de l'accès aux intruments logiciels, repose à
nouveaux frais le (vieux)  problème de la reproductibilité en SH.

Établir un parralèle entre la gestion de version et certaines
propositions de Gardin.

* Accès aux "données brutes"
* Diffusion et réutilisation des techniques ET des résultats
* Historique des changements


* principes : centralisation, cumulativité, automatisation
  (cf. principes dans Anarchéologie et rapports UNESCO)
Réalisations:
* fichier mécanographiques 
* réseaux documentaires (CRA, FRANTIQ, PACTOLS)

## Débats épistémologiques sur la reproduction des processus de recherche en sciences humaines

Mise en perspective théorique en reprenant l'historique des débats
épistémologique sur le statut de l'expérience et, partant la
reproduction, en sciences humaines.

(NB: j'ai ce qu'il faut sous le coude pour cela à partir du débat
Passeron-Gardin)


# Le versioning et la programmation lettrée en pratique

## état de l'art bibliographique 
 

2e moitié du XXe siècle : transferts de technologie depuis
l'informatique vers les sciences naturelles puis, plus récemment, les
sciences humaines.

Cette section se poursuivrait par une recension des publications
présentant ou analysant les technologies informatiques permettant le
suivi de versions et la reproductibilité des processus d'écriture en
sciences (humaines).


## Les instruments logiciels
Survol des instruments logiciels existant

* git
* mercurial
* svn
* CVS
* …

Que choisir ?

## Exemples applications

* cet article lui-même !
* Panormos : http://www.panormos.de/pp/
* Sebastiens Wiki (? non, dans la mesure  ou y a pas de programme
    lettrée, ni de suivi de version)
* Digging with CVS (je ne trouve pas sur internet)
* rkeos : https://gitlab.com/rkeos



# Conclusion

Je rassemble les points suivants en conclusion, ça me semble pas mal
de mettre ces considérations plus normatives qu'analytiques ici et non
dans le corps de l'article.

* Réflexion sur avantages / dangers
* Adoption du script et perspective d'avenir / Danger d'uniformisation ?
* Déontologie scientifique (reproductibilité + Science ouverte) 
* Pb transparence et erreurs dans un contexte de performance
    académique

    NB: j'ai récemment lu Nicolas Chevassus-au-Louis, Malscience, 2016
    C'est riche en biblio de scandales sur des pb de reproductibilité,
    surtout en sciences biomédicales mais pas que bien sûr.

# Idées d'illustrations
* Carte perforée: (NB: mieux encore, des illustrations de banques de
    données par Gardin, j'ai ce qu'il faut)
* "Click and Drag" (Screenshot 'Excel') (NB: tu m'expliqueras l'idée)
* GNU Make (NB: tu m'expliqueras l'idée)
* Script : (NB: redondant avec le suivant, cf commentaire ci dessous)
* Commentaire de script (NB: ok, ça peut être pas mal une illustration
    qui met en évidence les différentes types de textes — pour le
    coup, on peut même mettre une page de ma thèse : language naturel
    / code en LN non exécutable / code en LF exécutable)
* man man / Vignette (NB: ça a l'air rigolo mais je ne vois pas
    l'idée — celle d'introspection ? ça me plait)


-->
